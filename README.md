## Requirements
    Linux or Mac

## How to use

### Download tools
```
git clone --recurse-submodules https://gitlab.com/moonwaystar/ErfanGSIs.git -b aosp
cd ErfanGSIs
```

### For setting up requirements
    bash setup.sh

### Generating GSI from stock firmware URL
```
. build.sh
```